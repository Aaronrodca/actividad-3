using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveDoor : MonoBehaviour
{
    public bool puedeAbrir = false;
    public bool excepcion = false;
    private Animator animDoor;
    public Animator animLever;

    // Start is called before the first frame update

    void Start()
    {
        animDoor = GetComponent<Animator>();
    }
    void Update()
    {
        if (animLever.GetBool("active") == true)//Si la palanca esta arriba
        {
            if (Input.GetButtonDown("Interactive") && puedeAbrir == true)//Cuando presione E y este encima del trigger
            {
                animDoor.SetBool("character_nearby", true);
            }
            if (puedeAbrir == false)
            {
                animDoor.SetBool("character_nearby", false);
            }
        }
        else if (animLever.GetBool("active") == false && excepcion == false)//Si la palanca esta abajo
        {
            animDoor.SetBool("character_nearby", true);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            puedeAbrir = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            puedeAbrir = false;
        }
    }
}
