using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveLever : MonoBehaviour
{
    public bool puedeActivarse = false;
    public Animator animLever;
    private GameObject[] lucesEmergenciaPadre;
    private GameObject[] lucesPadre;

    // Start is called before the first frame update

    void Start()
    {
        lucesEmergenciaPadre = GameObject.FindGameObjectsWithTag("LucesEmergencia");
        lucesPadre = GameObject.FindGameObjectsWithTag("Luces");
    }
    void Update()
    {
        if (Input.GetButtonDown("Interactive") && puedeActivarse == true)
        {
            bool estadoActual = animLever.GetBool("active");
            animLever.SetBool("active", !estadoActual);

            if (estadoActual == true)
            {
                //Luces Normales
                foreach (GameObject padreL in lucesPadre)
                {
                    Light[] lucesHijas = padreL.GetComponentsInChildren<Light>();

                    foreach (Light puntoLuz in lucesHijas)
                    {
                        puntoLuz.intensity = 0f;
                    }
                }

                //Luces de Emergencia
                foreach (GameObject padreLE in lucesEmergenciaPadre)
                {
                    Animator[] lucesEmergenciaHijas = padreLE.GetComponentsInChildren<Animator>();

                    foreach (Animator puntoLuz in lucesEmergenciaHijas)
                    {
                        puntoLuz.SetBool("On", true);
                    }
                }
            }
            else if(estadoActual == false)
            {
                //Luces Normales
                foreach (GameObject padreL in lucesPadre)
                {
                    Light[] lucesHijas = padreL.GetComponentsInChildren<Light>();

                    foreach (Light puntoLuz in lucesHijas)
                    {
                        puntoLuz.intensity = 1.06f;
                    }
                }

                //Luces de Emergencia
                foreach (GameObject padreLE in lucesEmergenciaPadre)
                {
                    Animator[] lucesEmergenciaHijas = padreLE.GetComponentsInChildren<Animator>();

                    foreach (Animator puntoLuz in lucesEmergenciaHijas)
                    {
                        puntoLuz.SetBool("On", false);
                    }

                }
            }

        }

    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            puedeActivarse = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            puedeActivarse = false;
        }
    }
}
