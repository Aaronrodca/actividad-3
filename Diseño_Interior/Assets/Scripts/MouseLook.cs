using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public GameObject camerasParent;
    public float hRotationSpeed = 100f;
    public float vRotationSpeed = 80f;
    public float maxVerticalAngle;
    public float minVerticalAngle;
    public float smoothTime = 0.05f;

    float vCamRotationAngles;
    float hPlayerRotation;
    float currentHVelocity;
    float currentVVelocity;
    float targetCamEulers;
    Vector3 targetCamRotation;

    public float mouseSensitivity = 2.0f;
    public float upDownRange = 90.0f;

    private float verticalRotation = 0;
    private float smoothVerticalRotation = 0;
    private float verticalVelocity = 0;


    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        // Rotaci�n del personaje (horizontal)
        float rotLeftRight = Input.GetAxis("Mouse X") * mouseSensitivity;
        transform.Rotate(0, rotLeftRight, 0);

        // Rotaci�n de la c�mara (vertical)
        float rotUpDown = -Input.GetAxis("Mouse Y") * mouseSensitivity;
        verticalRotation += rotUpDown;
        verticalRotation = Mathf.Clamp(verticalRotation, -upDownRange, upDownRange);

        smoothVerticalRotation = Mathf.SmoothDamp(smoothVerticalRotation, verticalRotation, ref verticalVelocity, smoothTime);

        Camera.main.transform.localRotation = Quaternion.Euler(smoothVerticalRotation, 0, 0);
    }

    // Update is called once per frame
    public void handleRotation(float hInput, float vInput)
    {
        /*
        //GetRotation based on input
        float targetPlayerRotation = hInput * hRotationSpeed * Time.deltaTime;
        targetCamEulers += vInput * vRotationSpeed * Time.deltaTime;

        //Player Rotation
        hPlayerRotation = Mathf.SmoothDamp(hPlayerRotation, targetPlayerRotation, ref currentHVelocity, smoothTime);
        transform.Rotate(0f, hPlayerRotation, 0f);

        //Cam Rotation
        targetCamEulers = Mathf.Clamp(targetCamEulers, minVerticalAngle, maxVerticalAngle);
        vCamRotationAngles = Mathf.SmoothDamp(vCamRotationAngles, targetCamEulers, ref currentVVelocity, smoothTime);
        targetCamRotation.Set(-vCamRotationAngles, 0f, 0f);
        camerasParent.transform.localEulerAngles = targetCamRotation;*/
    }
}
