using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class SmoothFirstPersonController : MonoBehaviour
{
    public float movementSpeed = 5.0f;
    public float mouseSensitivity = 2.0f;
    public float upDownRange = 90.0f;
    public float smoothTime = 0.1f;
    public float jumpForce = 8.0f;
    public float runSpeedMultiplier = 2.0f;

    private CharacterController characterController;
    private float verticalRotation = 0;
    private float smoothVerticalRotation = 0;
    private float verticalVelocity = 0;
    private bool isGrounded;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        // Rotaci�n del personaje (horizontal)
        float rotLeftRight = Input.GetAxis("Mouse X") * mouseSensitivity;
        transform.Rotate(0, rotLeftRight, 0);

        // Rotaci�n de la c�mara (vertical)
        float rotUpDown = -Input.GetAxis("Mouse Y") * mouseSensitivity;
        verticalRotation += rotUpDown;
        verticalRotation = Mathf.Clamp(verticalRotation, -upDownRange, upDownRange);

        smoothVerticalRotation = Mathf.SmoothDamp(smoothVerticalRotation, verticalRotation, ref verticalVelocity, smoothTime);

        Camera.main.transform.localRotation = Quaternion.Euler(smoothVerticalRotation, 0, 0);

        // Salto
        if (isGrounded && Input.GetButtonDown("Jump"))
        {
            isGrounded = false;
            GetComponent<Rigidbody>().velocity = new Vector3(0, jumpForce, 0);
        }

        // Movimiento del personaje
        float forwardSpeed = Input.GetAxis("Vertical") * movementSpeed;
        float sideSpeed = Input.GetAxis("Horizontal") * movementSpeed;

        if (Input.GetKey(KeyCode.LeftShift)) // Si la tecla Shift est� presionada
        {
            forwardSpeed *= runSpeedMultiplier;
            sideSpeed *= runSpeedMultiplier;
        }

        Vector3 speed = new Vector3(sideSpeed, 0, forwardSpeed);
        speed = transform.rotation * speed;

        characterController.Move(speed * Time.deltaTime);
    }

    void FixedUpdate()
    {
        // Verificar si el personaje est� en el suelo
        RaycastHit hit;
        isGrounded = Physics.Raycast(transform.position, -Vector3.up, out hit, 1.1f);
    }
}


